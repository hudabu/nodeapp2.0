# Stage 1: Build
FROM node:20 AS build
WORKDIR /app
COPY package*.json ./
RUN npm ci --only=production
COPY . .

# Stage 2: Development
FROM build AS development
ENV NODE_ENV=development
RUN npm install --only=development

# Stage 3: Production
FROM node:20-slim AS production
WORKDIR /app
COPY --from=build /app .
ENV NODE_ENV=production
EXPOSE 1337
CMD ["node", "index.js"]
